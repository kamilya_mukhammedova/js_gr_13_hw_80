create schema MukhammedovaInventory collate utf8_general_ci;

use MukhammedovaInventory;

create table categories
(
    id          int auto_increment
        primary key,
    title       varchar(255) not null,
    description text         null
);

create table places
(
    id          int auto_increment
        primary key,
    title       varchar(255) not null,
    description text         null
);

create table items
(
    id          int auto_increment
        primary key,
    title       varchar(255) not null,
    category_id int          not null,
    place_id    int          null,
    description text         null,
    image       varchar(31)  null,
    constraint items_categories_id_fk
        foreign key (category_id) references categories (id),
    constraint items_places_id_fk
        foreign key (place_id) references places (id)
);

insert into categories (id, title, description)
values  (1, 'Furniture', 'Consists of large objects such as tables'),
        (2, 'Appliances', 'An instrument, apparatus, or device for a particular purpose or use. '),
        (3, 'Computer equipment', 'This includes, but is not limited to, laptops, monitors and display screens, “media,” keyboards, printers, modems and permanently installed wiring associated with such equipment.');

insert into places (id, title, description)
values  (1, 'Office 204', 'Main office on second floor.'),
        (2, 'Principal''s office', 'Private office of principal.'),
        (3, 'office 111', '');

insert into items (id, title, category_id, place_id, description, image)
values  (1, 'Macbook Air M1', 3, 2, '13.3-inch (diagonal) LED-backlit display with IPS technology; 2560-by-1600 native resolution at 227 pixels per inch with support for millions of colors', 'macbook.jpg'),
        (2, 'Table', 1, 1, 'an item of furniture with a flat top and one or more legs, used as a surface for working at, eating from or on which to place things.', 'table.jpg'),
        (3, 'TV set', 2, 2, 'Big black tv set', 'T7i6Dld_1-_PKd_A-sM4N.jpg'),
        (4, 'sound speaker', 2, 1, 'Red round', 'GqCNuTlC6DJ32qURQN-qP.jpg'),
        (5, 'sound speaker22', 2, 1, 'Red round', '_dnQPxUoFj4TovaCeEnbP.jpg');