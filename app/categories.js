const express = require('express');
const db = require('../mySqlDb');
const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    let query = 'SELECT * FROM categories';
    let [categories] = await db.getConnection().execute(query);
    for(let i = 0; i < categories.length; i++) {
      delete categories[i].description;
    }
    return res.send(categories);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const [categories] = await db.getConnection().execute('SELECT * FROM categories WHERE id = ?', [req.params.id]);
    const category = categories[0];
    if (!category) {
      return res.status(404).send({message: 'Category is not found'});
    }
    return res.send(category);
  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if(!req.body.title) {
      return res.status(400).send({message: 'Title of category is required'});
    }
    const newCategory = {
      title: req.body.title,
      description: req.body.description,
    };
    let query = 'INSERT INTO categories (title, description) VALUES (?,?)';
    const [results] = await db.getConnection().execute(query, [
      newCategory.title,
      newCategory.description,
    ]);
    newCategory.id = results.insertId;
    return res.send(newCategory);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    await db.getConnection().execute('DELETE FROM categories WHERE id = ?', [req.params.id]);
    return res.send({message: `Category with id ${req.params.id} has been deleted`});
  } catch (e) {
    next(e);
  }
});

module.exports = router;