const express = require('express');
const db = require('../mySqlDb');
const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    let query = 'SELECT * FROM places';
    let [places] = await db.getConnection().execute(query);
    for(let i = 0; i < places.length; i++) {
      delete places[i].description;
    }
    return res.send(places);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const [places] = await db.getConnection().execute('SELECT * FROM places WHERE id = ?', [req.params.id]);
    const place = places[0];
    if (!place) {
      return res.status(404).send({message: 'Place is not found'});
    }
    return res.send(place);
  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if(!req.body.title) {
      return res.status(400).send({message: 'Title of place is required'});
    }
    const newPlace= {
      title: req.body.title,
      description: req.body.description,
    };
    let query = 'INSERT INTO places (title, description) VALUES (?,?)';
    const [results] = await db.getConnection().execute(query, [
      newPlace.title,
      newPlace.description,
    ]);
    newPlace.id = results.insertId;
    return res.send(newPlace);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    await db.getConnection().execute('DELETE FROM places WHERE id = ?', [req.params.id]);
    return res.send({message: `Place with id ${req.params.id} has been deleted`});
  } catch (e) {
    next(e);
  }
});

module.exports = router;