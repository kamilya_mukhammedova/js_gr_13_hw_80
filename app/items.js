const express = require('express');
const db = require('../mySqlDb');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    let query = 'SELECT * FROM items';
    let [items] = await db.getConnection().execute(query);
    for(let i = 0; i < items.length; i++) {
      delete items[i].description;
      delete items[i].image;
    }
    return res.send(items);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const [items] = await db.getConnection().execute('SELECT * FROM items WHERE id = ?', [req.params.id]);
    const item = items[0];
    if (!item) {
      return res.status(404).send({message: 'Item is not found'});
    }
    return res.send(item);
  } catch (e) {
    next(e);
  }
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if(!req.body.title || !req.body.category_id || !req.body.place_id) {
      return res.status(400).send({message: 'Title of item, category id and place id are required'});
    }
    const newItem = {
      title: req.body.title,
      category_id: req.body.category_id,
      place_id: req.body.place_id,
      description: req.body.description,
      image: null,
    };
    if(req.file) {
      newItem.image = req.file.filename;
    }
    let query = 'INSERT INTO items (title, category_id, place_id, description, image) VALUES (?,?,?,?,?)';
    const [results] = await db.getConnection().execute(query, [
      newItem.title,
      newItem.category_id,
      newItem.place_id,
      newItem.description,
      newItem.image,
    ]);
    newItem.id = results.insertId;
    return res.send(newItem);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    await db.getConnection().execute('DELETE FROM items WHERE id = ?', [req.params.id]);
    return res.send({message: `Item with id ${req.params.id} has been deleted`});
  } catch (e) {
    next(e);
  }
});

module.exports = router;