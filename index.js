const express = require('express');
const app = express();
const db = require('./mySqlDb');
const categories = require('./app/categories');
const places = require('./app/places');
const items = require('./app/items');
const port = 8031;

app.use(express.json());
app.use(express.static('public'));
app.use('/categories', categories);
app.use('/places', places);
app.use('/items', items);

const run = async () => {
  await db.init();

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
};

run().catch(e => console.error(e));